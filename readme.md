## Requirements
- [Docker](https://docs.docker.com/install)
- [Docker Compose](https://docs.docker.com/compose/install)




## Setup for local development
1. Clone the repository.
2. Start the containers by running `docker-compose up -d` in the project root.
3. Install the composer packages by running `docker-compose exec laravel composer update`.
3. Build the database by running `docker-compose exec laravel php artisan migrate --seed`.
4. [If required] Run `php artisan user:make` to add a system user.
5. Access the Laravel instance on `http://127.0.0.1:${PROJECT_WEBHOST_PORT}`, refer .env file.
6. If there is a "Permission denied" error, run `docker-compose exec laravel chown -R www-data storage`.
7. To stop the application run `docker-compose down`

Note that the changes you make to local files will be automatically reflected in the container. since its working as a volume mount.



## Setup for production (Web-hosting)
1. Clone the repository.
2. Start the containers by running `docker-compose -f docker-compose.yml -f docker-compose.production.yml up -d --build --force-recreate  --always-recreate-deps` in the project root.
3. Composer will be installed via the Docker file.
4. Laravel's permissions will be setup via the Docker file.
5. To build the database run `docker-compose exec laravel bash -c 'yes | php artisan migrate'`.
6. [If required] Run `php artisan user:make` to add a system user.
7. Make sure the server's reverse-proxy is pointed to `http://127.0.0.1`
8. To stop the application run `docker-compose -f docker-compose.yml -f docker-compose.production.yml down`

### Demo hosted at: http://prettybooks.club/login