ARG LARAVEL_PATH=/var/www/html
ARG SRC=src
#........................................................................


FROM composer:2.0.13 AS composer
ARG LARAVEL_PATH
ARG SRC

#uncomment only if needed
#RUN composer self-update

COPY $SRC $LARAVEL_PATH

RUN rm -f $LARAVEL_PATH/.env

# uncomment if you dont want a fresh vendor set
#  to be installed every time a deployment is done.
#RUN rm -f $LARAVEL_PATH/vendor -r

COPY $SRC/.env.production $LARAVEL_PATH/.env

RUN COMPOSER_MEMORY_LIMIT=-1 composer update --working-dir $LARAVEL_PATH --ignore-platform-reqs --no-progress --optimize-autoloader
#........................................................................


FROM php:7.4-apache
ARG LARAVEL_PATH
ARG SRC


RUN apt-get update && apt-get install -y \
    libzip-dev \
    && docker-php-ext-install pdo_mysql \
    #&& mysql-client libmagickwand-dev --no-install-recommends \
    #&& pecl install imagick \
    #&& docker-php-ext-enable imagick \
    #&& docker-php-ext-install mcrypt \
    && docker-php-ext-install zip   


#required for int to text convertion in factories
RUN apt-get update && apt-get install -y zlib1g-dev libicu-dev g++ \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl

ENV APACHE_DOCUMENT_ROOT $LARAVEL_PATH/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

RUN a2enmod rewrite

COPY --from=composer /usr/bin/composer /usr/bin/composer
RUN mkdir -p /root/.composer
COPY --from=composer /tmp/cache /root/.composer/cache

RUN pecl install xdebug-2.9.2 \
	&& docker-php-ext-enable xdebug \
    && echo "xdebug.remote_enable=1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

WORKDIR $LARAVEL_PATH


#.....................................................................................

#Below process will be overriden due to volume mountting in local enviorenment
COPY --from=composer $LARAVEL_PATH $LARAVEL_PATH

RUN cd $LARAVEL_PATH && composer dump-autoload

RUN chown -R www-data $LARAVEL_PATH/storage

