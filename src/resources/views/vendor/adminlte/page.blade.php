@extends('adminlte::master')

@inject('layoutHelper', 'JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper')

@if($layoutHelper->isLayoutTopnavEnabled())
    @php( $def_container_class = 'container' )
@else
    @php( $def_container_class = 'container-fluid' )
@endif

@section('adminlte_css')
    @stack('css')
    @yield('css')
@stop

<style>
    .bg-light-purple,
    .sidebar-light-purple{
        background-color:#d9cef8 !important;
    }
</style>

@section('classes_body', $layoutHelper->makeBodyClasses())

@section('body_data', $layoutHelper->makeBodyData())

@section('body')
    <div class="wrapper">

        {{-- Top Navbar --}}
        @if($layoutHelper->isLayoutTopnavEnabled())
            @include('adminlte::partials.navbar.navbar-layout-topnav')
        @else
            @include('adminlte::partials.navbar.navbar')
        @endif

        {{-- Left Main Sidebar --}}
        @if(!$layoutHelper->isLayoutTopnavEnabled())
            @include('adminlte::partials.sidebar.left-sidebar')
        @endif

        {{-- Content Wrapper --}}
        <div class="content-wrapper {{ config('adminlte.classes_content_wrapper') ?? '' }}">

            {{-- Content Header --}}
            <div class="content-header">
                <div class="{{ config('adminlte.classes_content_header') ?: $def_container_class }}">
                    @yield('content_header')
                </div>
            </div>

            {{-- Main Content --}}
            <div class="content">
                <div class="{{ config('adminlte.classes_content') ?: $def_container_class }}">
                    @include('flash::message')

                    @yield('content')
                </div>
            </div>

        </div>

        {{-- Footer --}}
        @hasSection('footer')
            @include('adminlte::partials.footer.footer')
        @endif

        {{-- Right Control Sidebar --}}
        @if(config('adminlte.right_sidebar'))
            @include('adminlte::partials.sidebar.right-sidebar')
        @endif

    </div>
@stop

@section('adminlte_js')

    <script>
        {{-- Flasher auto-hide --}}
        $('div.alert').not('.alert-important').delay(4000).fadeOut(450);

        {{-- Search bar customizations --}}
        @if(request()->has('q') && !empty(request()->q))
            //set query value
            $('#nav-bar-search-input').val('{{request()->q}}');

            //trigger search field expansions
            $('#nav-bar-search-input').closest('li').find('a[data-widget="navbar-search"]').trigger('click');

            //on search close refresh page without the search param
            $('button[data-widget="navbar-search"]').on( "click", function() {
                location.href = '{{ request()->fullUrlWithQuery(request()->except('q')) }}';
            });
        @endif

    </script>

    @stack('js')
    @yield('js')
@stop
