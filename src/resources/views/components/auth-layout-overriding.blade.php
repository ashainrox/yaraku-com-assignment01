@push('css')
    <style>
        body {
            background: url('{{asset("assets/images/background-01.jpg")}}') no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .login-box .card,
        .register-box .card
        {
            opacity: 0.92;
        }


        .login-logo a,
        .register-logo a{
            color: darkred;
        }

        .card-body input[type=email],
        .card-body input[type=password],
        .card-body input[type=text]
        {
            font-weight: 600;
            font-style: italic;

        }

    </style>
@endpush
