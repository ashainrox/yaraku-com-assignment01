<!-- Action Button -->
<div class="form-group">
    @can("list-$resourceName")
        <a href="{{ route("{$routeBaseName}.index") }}" class="form-action-btn btn btn-outline-dark"><i class="fa fa-caret-left" aria-hidden="true"></i> Back</a>
    @endcan


    @can("delete-$resourceName")
        @if(isset($model))
            &nbsp;
            <button utton type='button' class="form-action-btn btn btn-outline-danger" onclick='tryDelete("Are you sure you wan to delete the {{$resourceName}}?")'>
                <i class="fa fa-trash" aria-hidden="true"></i> Delete
            </button>
        @endif
    @endcan


    @can("$actionPerforming-$resourceName")        &nbsp;
        <button class="form-action-btn btn btn-outline-success"><i class="fa fa-check" aria-hidden="true"></i> {{ucwords($actionPerforming)}}</button>
    @endcan
</div>
<!-- End Action Button -->


@can("delete-$resourceName")
    {{--hidden delete form--}}
    @push('hidden-content')
        @if(isset($model))
            {{Form::open(['url' => route("{$routeBaseName}.destroy", $model->id), 'method' => 'DELETE', 'style'=>"" ]) }}
                <button id="btn-hidden-form-delete"></button>
            {{Form::close()}}
        @endif
    @endpush
@endcan



{{--format form buttons--}}
@push('css')
    <style>
        .form-action-btn{
            margin-bottom: 1em;
        }
    </style>
@endpush


@can("delete-$resourceName")
    {{--delete function scripts--}}
    @push('js')
        @if( isset($model))
            <script>
                function tryDelete(message){
                    if(confirm(message)){
                        document.getElementById('btn-hidden-form-delete').click();
                    }
                }
            </script>
        @endif
    @endpush
@endcan
