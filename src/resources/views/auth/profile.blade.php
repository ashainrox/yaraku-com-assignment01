@extends('adminlte::page')

@section('title', $pageTitle)

@section('content_header')
    <h1>{{$pageTitle}}</h1>
@stop

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">

                        {{ Form::model($model, ['url' => route('profile.update'), 'method' => 'PATCH']) }}

                        <!-- Row -->
                            <div class="row">

                                <div class="col-md-12">
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label" for="name">Name</label>
                                        <div class="col-md-12">
                                            {{ Form::text('name', null, ['placeholder'=>"Name", 'class'=>"form-control input-md", 'required'=>""]) }}
                                            @include('components.field-error-msg', ['field'=>'name'])
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label" for="link">Email</label>
                                        <div class="col-md-12">
                                            {{ Form::text('email', null, ['placeholder'=>"Your Email", 'class'=>"form-control input-md"]) }}
                                            @include('components.field-error-msg', ['field'=>'email'])
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label" for="link">Password</label>
                                        <div class="col-md-12">
                                            {{ Form::password('password', ['placeholder'=>"New Password", 'class'=>"form-control input-md"]) }}
                                            @include('components.field-error-msg', ['field'=>'password'])
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label" for="link">Confirm Password</label>
                                        <div class="col-md-12">
                                            {{ Form::password('password_confirmation', ['placeholder'=>"Confirm Password", 'class'=>"form-control input-md"]) }}
                                            @include('components.field-error-msg', ['field'=>'password_confirmation'])
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- End Row -->

                            <div class="col-md-4 offset-md-4 col-sm-12">
                                <br>

                                <!-- Action Button -->
                                <div class="form-group">
                                    <a href="{{ route("profile.update") }}" class="form-action-btn btn btn-outline-dark"><i class="fa fa-caret-left" aria-hidden="true"></i> Back</a>

                                    <button class="form-action-btn btn btn-outline-success"><i class="fa fa-check" aria-hidden="true"></i> Update</button>
                                </div>
                                <!-- End Action Button -->

                            </div>

                        {{Form::close()}}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
