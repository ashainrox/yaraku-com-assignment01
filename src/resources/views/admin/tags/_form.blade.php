@extends('adminlte::page')

@section('title', $pageTitle)

@section('content_header')
    <h1>{{$pageTitle}}</h1>
@stop

@include('components.select2-libraries')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                    @if(isset($model))
                        {{ Form::model($model, ['url' => route($routeName, $model->id), 'method' => 'PATCH']) }}
                    @else
                        {{ Form::open(['url' => route($routeName), 'method' => 'POST']) }}
                    @endif


                    <!-- Row -->
                        <div class="row">

                            <!-- col-md-6 -->
                            <div class="col-md-6">

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-12 control-label" for="name">Name of The Tag</label>
                                    <div class="col-md-12">
                                        {{ Form::text('name', null, ['placeholder'=>"Tag Name", 'class'=>"form-control input-md", 'required'=>""]) }}
                                        @include('components.field-error-msg', ['field'=>'name'])
                                    </div>
                                </div>

                            </div>
                            <!-- End col-md-6 -->

                            <!-- col-md-6 -->
                            <div class="col-md-6">

                                <!-- Select Basic -->
                                <div class="form-group">
                                    <label class="col-md-12 control-label" for="books[]">Books</label>
                                    <div class="col-md-12">
                                        {{  Form::select('books[]', \App\Models\Book::all()->pluck('name', 'id'), null, ['class'=>"select2 form-control", 'multiple']) }}
                                        @include('components.field-error-msg', ['field'=>'books'])
                                    </div>
                                </div>

                            </div>
                            <!-- End col-md-6 -->

                            <div class="col-md-4 offset-md-4 col-sm-12">
                                <br>

                                @include('components.resource-form-buttons')

                            </div>

                        </div>
                        <!-- End Row -->


                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
