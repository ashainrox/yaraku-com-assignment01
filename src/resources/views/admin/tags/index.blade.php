@extends('adminlte::page')

@section('title', $pageTitle)

@section('content_header')
    <h1>{{$pageTitle}}</h1>
@stop

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        {{$models->links()}}

                        <table class="table">
                            <tr>
                                <th>Name</th>
                                <th>Books</th>
                            </tr>
                            @foreach($models as $Model)
                                <tr>
                                    <td><a href="{{route('tag.edit', $Model->id)}}">{{$Model->name}}</td>
                                    {{--TODO::add search filter in books for the tag--}}
                                    <td>{{$Model->books_count}}</td>
                                </tr>
                            @endforeach
                        </table>

                        {{$models->links()}}

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
