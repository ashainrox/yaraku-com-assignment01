@extends('adminlte::page')

@section('title', $pageTitle)

@section('content_header')
    <h1>{{$pageTitle}}</h1>
@stop

@include('components.select2-libraries')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        @if(isset($model))
                            {{ Form::model($model, ['url' => route($routeName, $model->id), 'method' => 'PATCH']) }}
                        @else
                            {{ Form::open(['url' => route($routeName), 'method' => 'POST']) }}
                        @endif


                            <!-- Row -->
                            <div class="row">

                                <!-- col-md-6 -->
                                <div class="col-md-6">
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label" for="isbn">ISBN (10/13)</label>
                                        <div class="col-md-12">
                                            {{ Form::text('isbn', null, ['placeholder'=>"ISBN Number", 'class'=>"form-control input-md", 'required'=>""]) }}
                                            @include('components.field-error-msg', ['field'=>'isbn'])
                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label" for="name">Name of The Book</label>
                                        <div class="col-md-12">
                                            {{ Form::text('name', null, ['placeholder'=>"Book Name", 'class'=>"form-control input-md", 'required'=>""]) }}
                                            @include('components.field-error-msg', ['field'=>'name'])
                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label" for="edition">Edition</label>
                                        <div class="col-md-12">
                                            {{ Form::text('edition', null, ['placeholder'=>"Edition of The Book", 'class'=>"form-control input-md"]) }}
                                            @include('components.field-error-msg', ['field'=>'edition'])
                                            <span class="help-block">Ex:-1st Edition, 2, third edition</span>
                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label" for="link">URL</label>
                                        <div class="col-md-12">
                                            {{ Form::text('link', null, ['placeholder'=>"A link to the book", 'class'=>"form-control input-md"]) }}
                                            @include('components.field-error-msg', ['field'=>'link'])
                                            <span class="help-block">Ex:- https://amazon.com/book/1323</span>
                                        </div>
                                    </div>


                                    <!-- Select Basic -->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label" for="status">Active Status</label>
                                        <div class="col-md-4 col-sm-12">
                                            {{  Form::select('status', \App\Models\Book::STATUS_OPTIONS, null, ['class'=>"form-control"]) }}
                                            @include('components.field-error-msg', ['field'=>'status'])
                                        </div>
                                    </div>

                                </div>
                                <!-- End col-md-6 -->

                                <!-- col-md-6 -->
                                <div class="col-md-6">

                                {{--                                    <!-- File Button -->--}}
                                {{--                                    <div class="form-group">--}}
                                {{--                                        <label class="col-md-12 control-label" for="image">Image</label>--}}
                                {{--                                        <div class="col-md-12">--}}
                                {{--                                            <input id="image" name="image" class="input-file" type="file">--}}
                                {{--                                        </div>--}}
                                {{--                                    </div>--}}

                                    <!-- Textarea -->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label" for="description">Description</label>
                                        <div class="col-md-12">
                                            {{ Form::textarea('description', null, ['placeholder'=>"Description of The Book", 'rows'=>5,'class'=>"form-control"]) }}
                                            @include('components.field-error-msg', ['field'=>'description'])
                                        </div>
                                    </div>

                                    <!-- Select Basic -->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label" for="authors[]">Authors</label>
                                        <div class="col-md-12">
                                            {{  Form::select('authors[]', \App\Models\Author::all()->pluck('name', 'id'), null, ['class'=>"select2 form-control", 'multiple']) }}
                                            @include('components.field-error-msg', ['field'=>'authors'])
                                        </div>
                                    </div>

                                    <!-- Select Basic -->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label" for="tags">Tags</label>
                                        <div class="col-md-12">
                                            {{  Form::select('tags[]', \App\Models\Tag::all()->pluck('name', 'id'), null, ['class'=>"select2 form-control", 'multiple']) }}
                                            @include('components.field-error-msg', ['field'=>'tags'])
                                        </div>
                                    </div>

                                </div>
                                <!-- End col-md-6 -->

                                <div class="row">

                                </div>

                                <div class="col-md-4 offset-md-4 col-sm-12">
                                    <br>

                                    @include('components.resource-form-buttons')

                                </div>

                            </div>
                            <!-- End Row -->


                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
