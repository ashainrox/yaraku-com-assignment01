@extends('adminlte::page')

@section('title', $pageTitle)

@section('content_header')
    <h1>{{$pageTitle}}</h1>
@stop

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        @if(isset($model))
                            {{ Form::model($model, ['url' => route($routeName, $model->id), 'method' => 'PATCH']) }}
                        @else
                            {{ Form::open(['url' => route($routeName), 'method' => 'POST']) }}
                        @endif


                            <!-- Row -->
                            <div class="row">

                                <!-- col-md-6 -->
                                <div class="col-md-6">

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label" for="name">Name of The Author</label>
                                        <div class="col-md-12">
                                            {{ Form::text('name', null, ['placeholder'=>"Author Name", 'class'=>"form-control input-md", 'required'=>""]) }}
                                            @include('components.field-error-msg', ['field'=>'name'])
                                        </div>
                                    </div>

                                                                        <!-- Textarea -->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label" for="description">Description</label>
                                        <div class="col-md-12">
                                            {{ Form::textarea('description', null, ['placeholder'=>"Description of The Author", 'rows'=>5,'class'=>"form-control"]) }}
                                            @include('components.field-error-msg', ['field'=>'description'])
                                        </div>
                                    </div>

                                </div>
                                <!-- End col-md-6 -->

                                <!-- col-md-6 -->
                                <div class="col-md-6">

                                {{--                                    <!-- File Button -->--}}
                                {{--                                    <div class="form-group">--}}
                                {{--                                        <label class="col-md-12 control-label" for="image">Image</label>--}}
                                {{--                                        <div class="col-md-12">--}}
                                {{--                                            <input id="image" name="image" class="input-file" type="file">--}}
                                {{--                                        </div>--}}
                                {{--                                    </div>--}}

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label" for="link">URL</label>
                                        <div class="col-md-12">
                                            {{ Form::text('link', null, ['placeholder'=>"A link to the author", 'class'=>"form-control input-md"]) }}
                                            @include('components.field-error-msg', ['field'=>'link'])
                                            <span class="help-block">Ex:- https://www.famousauthors.org/agatha-christie</span>
                                        </div>
                                    </div>

                                    <!-- Select Basic -->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label" for="books[]">Books</label>
                                        <div class="col-md-12">
                                            {{  Form::select('books[]', \App\Models\Book::all()->pluck('name', 'id'), null, ['class'=>"select2 form-control", 'multiple']) }}
                                            @include('components.field-error-msg', ['field'=>'books'])
                                        </div>
                                    </div>

                                </div>
                                <!-- End col-md-6 -->

                                <div class="col-md-4 offset-md-4 col-sm-12">
                                    <br>

                                    @include('components.resource-form-buttons')

                                </div>

                            </div>
                            <!-- End Row -->


                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
