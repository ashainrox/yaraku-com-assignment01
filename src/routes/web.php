<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return response()->redirectTo('login');
});

//Auth & account related URLs
Auth::routes();
Route::get('account/profile', 'ProfileController@showProfile')->name('profile.show');
Route::patch('account/profile', 'ProfileController@updateProfile')->name('profile.update');


Route::get('dashboard', 'DashboardController')->name('dashboard')->middleware('auth');

//books
Route::get('book/xml', 'Admin\BookController@xmlDownload')->name('book.xml');
Route::get('book/csv', 'Admin\BookController@csvDownload')->name('book.csv');
Route::resource('book', 'Admin\BookController')->except('show');

//author
Route::get('author/xml', 'Admin\AuthorController@xmlDownload')->name('author.xml');
Route::get('author/csv', 'Admin\AuthorController@csvDownload')->name('author.csv');
Route::resource('author', 'Admin\AuthorController')->except('show');

Route::resource('tag', 'Admin\TagController')->except('show');
