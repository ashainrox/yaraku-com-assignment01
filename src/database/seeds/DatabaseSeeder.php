<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);

        factory(App\Models\Book::class, 50)->create()->each(function ($book) {
            $book->authors()->createMany(
                factory(App\Models\Author::class, random_int(1,3))->make()->toArray()
            );
            $book->tags()->createMany(
                factory(App\Models\Tag::class, random_int(0,5))->make()->toArray()
            );
        });
    }
}
