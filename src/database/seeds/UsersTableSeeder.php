<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate([
            'email' => 'ashainrox@gmail.com',
        ],[
            'name' => "Super Admin",
            'email_verified_at' => now(),
            'password' => Hash::make('123456'),
            'role' => User::ROLE_OPTION__ADMIN,
        ]);
    }
}
