<?php

namespace App\Models;

use App\Components\SearchEveryWhere;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Author extends Model
{
    use SoftDeletes, SearchEveryWhere;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'link',
    ];

    /**
     * Columns white listed for the global search
    */
    protected $searchable = [
        'name', 'description'
    ];


    /**
     * Get the books belonging to the author.
     */
    public function books(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Book::class);
    }
}
