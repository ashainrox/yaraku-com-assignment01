<?php

namespace App\Models;

use App\Components\SearchEveryWhere;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use SoftDeletes, SearchEveryWhere;

    const STATUS_OPTION__ACTIVE = 1;
    const STATUS_OPTION__DISABLED = 0;

    const STATUS_OPTIONS = [
        self::STATUS_OPTION__ACTIVE => 'Active',
        self::STATUS_OPTION__DISABLED => 'Inactive',
    ];

    const STATUS_HTML_OPTIONS = [
        self::STATUS_OPTION__ACTIVE => '<i class=" fas fa-dot-circle text-success"></i>',
        self::STATUS_OPTION__DISABLED => '<i class="fas fa-dot-circle text-danger"></i>'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'isbn', 'name', 'description', 'edition', 'link', 'status'
    ];


    /**
     * Columns white listed for the global search
     */
    protected $searchable = [
        'isbn', 'name', 'description', 'edition',
    ];


    /**
     * Get the authors for the book.
     */
    public function authors(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Author::class);
    }


    /**
     * Get the tags for the book.
     */
    public function tags(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Tag::class);
    }

    public function status(bool $asHtml = false){
        return ($asHtml?self::STATUS_HTML_OPTIONS:self::STATUS_OPTIONS)[$this->status];
    }
}
