<?php


namespace App\Components;


trait SearchEveryWhere
{
    public function scopeSearchInColumns($query, string $needle = null, array $columns = []){
        //enable search bar on the adminLte UI
        enableAdminLteSearch();

        if($needle){//if a needle exists

            if(!count($columns)){ //if columns are not passed to the function

                if(property_exists ( $this , 'searchable' ) ){ //check if a property 'searchable' exist locally
                    $columns = $this->searchable;
                }else{
                    //do not search
                    return $query;
                }
            }

            foreach ($columns as $column){
                $query->orWhere($column, 'LIKE', "%{$needle}%");
            }
        }

        return $query;
    }
}
