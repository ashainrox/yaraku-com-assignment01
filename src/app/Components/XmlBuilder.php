<?php


namespace App\Components;


use Carbon\Carbon;
use Spatie\ArrayToXml\ArrayToXml;
use Illuminate\Database\Eloquent\Builder;

class XmlBuilder
{
    /**
     * XML library reference https://github.com/spatie/array-to-xml
     * This class implements the XML `spatie/array-to-xml` library providing a generic interface to be used wih eloquent
    */

    private $resourceName;

    private $fileName;

    protected $query;

    protected $queryMapFunction;

    protected $resourceUrl;



    public function __construct(string $resourceName, Builder $query, $closure = null, string $url = null, string $fileName = null){

        $this->resourceName = $resourceName;

        $this->query = $query;

        $this->query = $query;

        $this->queryMapFunction = $closure;

        $this->resourceUrl = $url;


        //download file name
        if($fileName){
            $this->fileName = $fileName;
        }else{
            $this->fileName = config('app.name') . " - " . ucwords($this->resourceName) . "-Collection  " . Carbon::now() . '.xml';
        }
    }



    /**
     * Prepare XML instance
    */
    private function getContent(): ArrayToXml{

        //get the collection
        $collection = $this->query->get();

        //modifying the collection via a map function if queryMapFunction is set
        if($this->queryMapFunction){
            //modifying the collection, for flexibility during a XML import|rendering|reading
            $collection = $collection->map($this->queryMapFunction);
        }


        //preparing an array to the required standard as expected by the library
        $dataSet = [
            $this->resourceName => $collection->toArray()
        ];



        //Setting up root node with custom name
        $rootParams = [
            'rootElementName' => \Str::pluralStudly($this->resourceName),
        ];

        //Setting custom properties for the root node, added resource URL if exists.
        if($this->resourceUrl){
            $rootParams['_attributes'] = [
                'xmlns' => $this->resourceUrl,
            ];
        }



        //initializing the XML instance with the data set and custom properties
        return new ArrayToXml(
            $dataSet,
            $rootParams
//            ,true,
//            'UTF-8',
//            '1.1',
//            [],
//            true
        );
    }




    /**
     * Get the XML as a download response
    */
    public function getFile(): \Illuminate\Http\Response
    {

        //converting the XML instance to string
        $resultContent = $this->getContent()->toXml();

        //return a download response
        $response = \Illuminate\Http\Response::create($resultContent, 200);
        $response->header('Content-Type', 'text/xml');
        $response->header('Cache-Control', 'public');
        $response->header('Content-Description', 'File Transfer');
        $response->header('Content-Disposition', 'attachment; filename=' . $this->fileName . '');
        $response->header('Content-Transfer-Encoding', 'binary');

        return $response;
    }




    /**
     * Get the XML as a string
    */
    public function getAsString(): string {
        return $this->getContent()->prettify()->toXml();
    }


}
