<?php


namespace App\Components;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class CsvBuilder
{

    /**
     * Example take from: https://www.laravelcode.com/post/how-to-export-csv-file-in-laravel-example
     * This class implements the CSV as module providing a generic interface to be used wih eloquent
    */

    private $resourceName;

    private $fileName; //download file name

    protected $query; //base query to executed to get the data set

    protected $queryMapFunction; //a closure function to be used inside the map function to do any modification to the collection before export



    public function __construct(string $resourceName, Builder $query, $closure = null, string $fileName = null){

        $this->resourceName = $resourceName;

        $this->query = $query;

        $this->query = $query;

        $this->queryMapFunction = $closure;

        //download file name
        if($fileName){
            $this->fileName = $fileName;
        }else{
            $this->fileName = config('app.name') . " - " . ucwords($this->resourceName) . "-Collection  " . Carbon::now() . '.csv';
        }
    }




    /**
     * Prepare XML instance
     */
    private function getContent()
    {
        //get the collection
        $collection = $this->query->get();

        //modifying the collection via a map function if queryMapFunction is set
        if($this->queryMapFunction){
            //modifying the collection, for flexibility during a XML import|rendering|reading
            $collection = $collection->map($this->queryMapFunction);
        }

        $columns = array_keys($collection?$collection->first()->toArray():[]);
        $collection = $collection->toArray();

        $callback = function() use($collection, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($collection as $model) {
                fputcsv($file, $model);
            }

            fclose($file);
        };

        return $callback;
    }




    /**
     * Get the CSV as a download response
     */
    public function getFile(): \Symfony\Component\HttpFoundation\StreamedResponse
    {
        //return a download response
        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$this->fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );
        return response()->stream($this->getContent(), 200, $headers);
    }




    /**
     * Get the CSV as a string
     */
    public function getAsString(): string {
        return $this->getContent();
    }

}
