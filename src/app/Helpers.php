<?php
/**
 * This file contains custom helper functions used through out the application
*/


/**
 * Convert a number in it's ordinal form but also in words.
 * Ex:- 10 = tenth, 20 =twentieth, 15 = fifteenth
 *
 * @param int $number
 * @return \Illuminate\Http\Response
*/
function ordinalNumbersInWords(int $number): string {
    $formatter = new \NumberFormatter('en_US', \NumberFormatter::SPELLOUT);
    $formatter->setTextAttribute(\NumberFormatter::DEFAULT_RULESET, "%spellout-ordinal");
    return $formatter->format($number);
}

function isAdmin(): bool{
    return auth()->check() && auth()->user()->isAdmin();
}

function isCashier(): bool{
    return auth()->check() && auth()->user()->isCashier();
}


/**
 * Find the index/key where the search configuration is set and enable it
*/
function enableAdminLteSearch(): ?int{

    //enable the top-nav search if this middleware is enables
    foreach (config('adminlte.menu') as $key => $param){
        if( isset($param["type"]) &&  $param["type"] == "navbar-search" ){

            config(["adminlte.menu.{$key}.topnav_right" => true]); //override configuration

            //return the index
            return $key;
        }
    }
}
