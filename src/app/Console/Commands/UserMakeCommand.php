<?php

namespace App\Console\Commands;


use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
//use Silber\Bouncer\Database\Role;

class UserMakeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:make';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new user';

    protected $rolesMap = [];

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {

        //set user roles for later use
        $this->rolesMap = User::ROLE_OPTIONS;


        do {
            $details  = $this->askForUserDetails($details ?? null);
            $name     = $details['name'];
            $email    = $details['email'];
            $password = $details['password'];
            $role     = array_search($details['role'],$this->rolesMap,true); //get index of selection

        } while (!$this->confirm("Create user {$name} <{$email}>?", true));


        //push data to db
        $user = User::forceCreate([
            'name' => $name,
            'email' => $email,
            'password' => \Hash::make($password),
            'role' => $role,
            'email_verified_at' => Carbon::now()
        ]);


        //respond
        $this->info("Created new user #{$user->id}: {$name}");
    }

    /**
     * @param null $defaults
     * @return array
     */
    protected function askForUserDetails($defaults = null)
    {
        $name     = $this->ask('Full name of user?', $defaults['name'] ?? null);
        $email    = $this->askUniqueEmail('Email address for user?', $defaults['email'] ?? null);
        $password = $this->secret('Password for user?', $defaults['password'] ?? null);
        $role     = $this->choice('Which role should this user have?', $this->rolesMap, $defaults['role'] ?? null);

        return compact('name', 'email', 'password', 'role');
    }

    /**
     * @param      $message
     * @param null $default
     * @return string
     */
    protected function askUniqueEmail($message, $default = null)
    {
        do {
            $email = $this->ask($message, $default);
        } while (!$this->checkEmailIsValid($email) || !$this->checkEmailIsUnique($email));

        return $email;
    }

    /**
     * @param $email
     * @return bool
     */
    protected function checkEmailIsValid($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->error('Sorry, "' . $email . '" is not a valid email address!');
            return false;
        }

        return true;
    }

    /**
     * @param $email
     * @return bool
     */
    public function checkEmailIsUnique($email)
    {
        if ($existingUser = User::whereEmail($email)->first()) {
            $this->error('Sorry, "' . $existingUser->email . '" is already in use by ' . $existingUser->name . '!');
            return false;
        }

        return true;
    }
}
