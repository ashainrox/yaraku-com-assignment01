<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();


        //Dashboard
        Gate::define('view-dashboard', function (User $user){
            return $user->isCashier() || $user->isAdmin();
        });


        //Books
        Gate::define('list-book', function (User $user){
            return $user->isCashier() || $user->isAdmin();
        });

        Gate::define('xml-book', function (User $user){
            return $user->isAdmin();
        });

        Gate::define('csv-book', function (User $user){
            return $user->isAdmin();
        });

        Gate::define('view-book', function (User $user){
            return $user->isCashier() || $user->isAdmin();
        });

        Gate::define('create-book', function (User $user){
            return $user->isAdmin();
        });

        Gate::define('update-book', function (User $user){
            return $user->isAdmin();
        });

        Gate::define('delete-book', function (User $user){
            return $user->isAdmin();
        });


        //Author Gates
        Gate::define('list-author', function (User $user){
            return $user->isCashier() || $user->isAdmin();
        });

        Gate::define('xml-author', function (User $user){
            return $user->isAdmin();
        });

        Gate::define('csv-author', function (User $user){
            return $user->isAdmin();
        });

        Gate::define('view-author', function (User $user){
            return $user->isCashier() || $user->isAdmin();
        });

        Gate::define('create-author', function (User $user){
            return $user->isAdmin();
        });

        Gate::define('update-author', function (User $user){
            return $user->isAdmin();
        });

        Gate::define('delete-author', function (User $user){
            return $user->isAdmin();
        });


        //Tags
        Gate::define('list-tag', function (User $user){
            return $user->isCashier() || $user->isAdmin();
        });

        Gate::define('view-tag', function (User $user){
            return $user->isCashier() || $user->isAdmin();
        });

        Gate::define('create-tag', function (User $user){
            return $user->isAdmin();
        });

        Gate::define('update-tag', function (User $user){
            return $user->isAdmin();
        });

        Gate::define('delete-tag', function (User $user){
            return $user->isAdmin();
        });
    }
}
