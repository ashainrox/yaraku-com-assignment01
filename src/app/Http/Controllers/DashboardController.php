<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\VerificationController;
use App\Models\Author;
use App\Models\Book;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class DashboardController extends VerificationController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function __invoke()
    {
        if (! Gate::allows('view-dashboard')) {
            abort(403);
        }

        return view('dashboard', [
            'pageTitle' => 'Dashboard',
            'bookCount' => Book::count(),
            'tagCount' => Tag::count(),
            'authorCount' => Author::count(),
        ]);
    }
}
