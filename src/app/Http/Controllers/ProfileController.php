<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\VerificationController;
use Illuminate\Http\Request;

class ProfileController extends VerificationController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showProfile()
    {
        return view('auth.profile', [
            'pageTitle' => 'Profile',
            'model' => \Auth::user(),
        ]);
    }

    public function updateProfile(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,id,'.\Auth::id()],
            'password' => ['nullable', 'string', 'min:8', 'confirmed'],
        ]);

        $user = \Auth::user();
        $user->name = $request->name;

        if( $request->has('password') && !is_null($request->password) ){//encrypt password if exists
            $user->password = \Hash::make($request->password);
        }


        if($user->isDirty()){

            if( $user->save() ){ //try to save
                flash('<b>Success!</b> Profile was update!')->success();
                return redirect()->route('profile.show');
            }else{
                flash('<b>Opps!</b> Something went wrong!')->error();
                return back()->withInput();
            }

        }else{
            flash('No changes detected!')->warning();
            return back()->withInput();
        }
    }
}
