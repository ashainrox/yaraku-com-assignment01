<?php

namespace App\Http\Controllers\Admin;

use App\Components\CsvBuilder;
use App\Components\XmlBuilder;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Requests\AuthorRequest;
use App\Models\Author;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;

class AuthorController extends VerificationController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (! Gate::allows('list-author')) {
            abort(403);
        }

        return view('admin.authors.index', [
            'pageTitle' => 'Authors',
            'models' => Author::latest()->searchInColumns($request->q)->paginate()
        ]);
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function xmlDownload(Request $request): Response
    {
        if (! Gate::allows('xml-author')) {
            abort(403);
        }

        if(Author::count()==0){
            flash('<b>Warning!</b> You don`t have any authors yet!')->warning();
            return back();
        }

        $xml = new XmlBuilder(

            //Generic name
            'author',

            //Query to get the collection
            Author::query(),

            //injecting `id` as attributes to each row node, for flexibility during a XML import|rendering|reading
            function ($model, $key){

                //node attributes per each row
                $model->_attributes = [
                    'id' => $model->id
                ]; //TODO:: convert status columns

                return $model;
            },

            //URL reference
            route('author.index')

        );

        //return a download response
        return $xml->getFile();
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function csvDownload(Request $request)
    {
        if (! Gate::allows('csv-author')) {
            abort(403);
        }


        if(Author::count()==0){
            flash('<b>Warning!</b> You don`t have any authors yet!')->warning();
            return back();
        }

        $csv = new CsvBuilder(

            //Generic name
            'author',

            //Query to get the collection
            Author::query()
        );

        //return a download response
        return $csv->getFile();

    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('create-author')) {
            abort(403);
        }

        return view('admin.authors._form', [
            'pageTitle' => "Create Author", //page title

            'routeBaseName' => 'author',
            'routeName' => 'author.store',

            'resourceName' => 'author',

            'actionPerforming' => 'create'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AuthorRequest $request)
    {
        if (! Gate::allows('create-author')) {
            abort(403);
        }

        $author = Author::Create($request->only('name', 'description' ,'link'));

        if( $author ){

            $author->books()->sync($request->books);

            flash('<b>Success!</b> Author was created!')->success();
            return redirect()->route('author.edit', $author->id);
        }else{
            flash('<b>Opps!</b> Something went wrong!')->error();
            return back()->withInput();
        }
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('view-author')) {
            abort(403);
        }

        return view('admin.authors._form', [
            'pageTitle' => "Update Author", //page title

            'model' => Author::findOrFail($id),

            'routeBaseName' => 'author',
            'routeName' => 'author.update', //

            'resourceName' => 'author',

            'actionPerforming' => 'update'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AuthorRequest $request, $id)
    {
        if (! Gate::allows('update-author')) {
            abort(403);
        }

        $author = Author::findOrFail($id);
        $author->fill($request->only('name', 'description' ,'link'));

        if( $author->save() ){ //try to save

            $author->books()->sync($request->books);

            flash('<b>Success!</b> Author was update!')->success();
            return redirect()->route('author.edit', $id);
        }else{
            flash('<b>Opps!</b> Something went wrong!')->error();
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('delete-author')) {
            abort(403);
        }

        if( Author::findOrFail($id)->delete() ){
            flash('<b>Success!</b> Author was deleted!')->success();
            return redirect()->route('author.index');
        }else{
            flash('<b>Opps!</b> Something went wrong!')->error();
            return back()->withInput();
        }
    }
}
