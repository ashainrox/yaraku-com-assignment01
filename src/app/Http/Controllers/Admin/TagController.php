<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Auth\VerificationController;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class TagController extends VerificationController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (! Gate::allows('list-tag')) {
            abort(403);
        }

        return view('admin.tags.index', [
            'pageTitle' => 'Tags',
            'models' => Tag::withCount('books')->latest()->searchInColumns($request->q)->paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (! Gate::allows('create-tag')) {
            abort(403);
        }

        return view('admin.tags._form', [
            'pageTitle' => "Create Tag", //page title

            'routeBaseName' => 'tag',
            'routeName' => 'tag.store',

            'resourceName' => 'tag',

            'actionPerforming' => 'create'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('create-tag')) {
            abort(403);
        }

        $tag = Tag::Create($request->only('name'));

        if( $tag ){

            $tag->books()->sync($request->books);

            flash('<b>Success!</b> Tag was created!')->success();
            return redirect()->route('tag.edit', $tag->id);
        }else{
            flash('<b>Opps!</b> Something went wrong!')->error();
            return back()->withInput();
        }
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('view-tag')) {
            abort(403);
        }

        return view('admin.tags._form', [
            'pageTitle' => "Update Tag", //page title

            'model' => \App\Models\Tag::findOrFail($id),

            'routeBaseName' => 'tag',
            'routeName' => 'tag.update', //

            'resourceName' => 'tag',

            'actionPerforming' => 'update'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('update-tag')) {
            abort(403);
        }

        $tag = Tag::findOrFail($id);
        $tag->fill($request->only('name'));

        if( $tag->save() ){ //try to save

            $tag->books()->sync($request->books);

            flash('<b>Success!</b> Tag was update!')->success();
            return redirect()->route('tag.edit', $id);
        }else{
            flash('<b>Opps!</b> Something went wrong!')->error();
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('delete-tag')) {
            abort(403);
        }

        if( Tag::findOrFail($id)->delete() ){
            flash('<b>Success!</b> Tag was deleted!')->success();
            return redirect()->route('tag.index');
        }else{
            flash('<b>Opps!</b> Something went wrong!')->error();
            return back()->withInput();
        }
    }
}
