<?php

namespace App\Http\Controllers\Admin;

use App\Components\CsvBuilder;
use App\Components\XmlBuilder;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Requests\BookRequest;
use App\Models\Book;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\StreamedResponse;

class BookController extends VerificationController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if (! Gate::allows('list-book')) {
            abort(403);
        }

        $searchColumns = [];

        $searchValue = $request->q; //make a copy, since it might need some cleaning and we dont want to mutate the original request

        if( $request->has('q') && \Str::startsWith($searchValue, 'isbn::') ){ //if search has prefix

            $searchColumns[] = 'isbn'; //add isbn as columns to be searched
            $searchValue = ltrim($searchValue, 'isbn::'); //remove the prefix to get the exact value
        }

        return view('admin.books.index', [
            'pageTitle' => 'Books',
            'models' => Book::latest()->searchInColumns($searchValue, $searchColumns)->paginate()
        ]);
    }



    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function xmlDownload(Request $request): Response
    {
        if (! Gate::allows('xml-book')) {
            abort(403);
        }

        if(Book::count()==0){
            flash('<b>Warning!</b> You don`t have any books yet!')->warning();
            return back();
        }

        $xml = new XmlBuilder(

                //Generic name
                'book',

                //Query to get the collection
                \App\Models\Book::query(),

                //injecting `id` & `isbn` as attributes to each row node, for flexibility during a XML import|rendering|reading
                function ($model, $key){

                    //node attributes per each row
                    $model->_attributes = [
                        'id' => $model->id,
                        'isbn' => $model->isbn
                    ]; //TODO:: convert status columns

                    return $model;
                },

                //URL reference
                route('book.index')

            );

        //return a download response
        return $xml->getFile();

    }



    /**
     * Display a listing of the resource.
     *
     * @return StreamedResponse
     */
    public function csvDownload(Request $request)
    {
        if (! Gate::allows('csv-book')) {
            abort(403);
        }

        if(Book::count()==0){
            flash('<b>Warning!</b> You don`t have any books yet!')->warning();
            return back();
        }

        $csv = new CsvBuilder(

            //Generic name
            'book',

            //Query to get the collection
            Book::query()
        );

        //return a download response
        return $csv->getFile();

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if (! Gate::allows('create-book')) {
            abort(403);
        }

        return view('admin.books._form', [
            'pageTitle' => "Create Book", //page title

            'routeBaseName' => 'book',
            'routeName' => 'book.store',

            'resourceName' => 'book',

            'actionPerforming' => 'create'
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param BookRequest $request
     * @return Response
     */
    public function store(BookRequest $request)
    {
        if (! Gate::allows('create-book')) {
            abort(403);
        }

        $book = Book::Create($request->only('isbn', 'name', 'description' ,'edition' ,'link' ,'status'));

        if( $book ){

            $book->authors()->sync($request->authors);
            $book->tags()->sync($request->tags);

            flash('<b>Success!</b> Book was created!')->success();
            return redirect()->route('book.edit', $book->id);
        }else{
            flash('<b>Opps!</b> Something went wrong!')->error();
            return back()->withInput();
        }
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if (! Gate::allows('view-book')) {
            abort(403);
        }

        return view('admin.books._form', [
            'pageTitle' => "Update Book", //page title

            'model' => \App\Models\Book::findOrFail($id),

            'routeBaseName' => 'book',
            'routeName' => 'book.update', //

            'resourceName' => 'book',

            'actionPerforming' => 'update'
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param BookRequest $request
     * @param int $id
     * @return Response
     */
    public function update(BookRequest $request, $id)
    {
        if (! Gate::allows('update-book')) {
            abort(403);
        }

        $book = Book::findOrFail($id);
        $book->fill($request->only('isbn', 'name', 'description' ,'edition' ,'link' ,'status'));

        if( $book->save() ){ //try to save

            $book->authors()->sync($request->authors);
            $book->tags()->sync($request->tags);

            flash('<b>Success!</b> Book was update!')->success();
            return redirect()->route('book.edit', $id);
        }else{
            flash('<b>Opps!</b> Something went wrong!')->error();
            return back()->withInput();
        }
    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        if (! Gate::allows('delete-book')) {
            abort(403);
        }

        if( Book::findOrFail($id)->delete() ){
            flash('<b>Success!</b> Book was deleted!')->success();
            return redirect()->route('book.index');
        }else{
            flash('<b>Opps!</b> Something went wrong!')->error();
            return back()->withInput();
        }
    }
}
