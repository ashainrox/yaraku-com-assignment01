<?php

namespace App\Http\Requests;

use App\Models\Book;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'isbn' => "ISBN"
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'isbn' => 'required|min:10|max:13', //TODO::create a separate ISBN rule
            'name' => 'required|max:500|min:2',
            'description' => 'nullable|min:2|max:3000',
            'edition' => 'nullable|min:2|max:200',
            'link' => 'nullable|url|max:800',
            'status' => Rule::in(array_keys(Book::STATUS_OPTIONS)),

            //author relation
            'authors' => 'array',
            'authors.*' => ['numeric', Rule::exists(\App\Models\Author::class,'id')],

            //tag relation
            'tags' => 'array',
            'tags.*' => ['numeric', Rule::exists(\App\Models\Tag::class,'id')],
        ];
    }
}
