<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function check_if_books_are_allowed_to_admin()
    {
        $this->actingAs(factory(\App\Models\User::class)->states('admin')->make()); //as an admin

        $response = $this->get('/book/create');

        $response->assertStatus(200);
    }


    public function check_if_books_are_allowed_to_cashier()
    {
        $this->actingAs(factory(\App\Models\User::class)->states('cashier')->make()); //as an admin

        $response = $this->get('/book/create');

        $response->assertStatus(403);
    }
}
