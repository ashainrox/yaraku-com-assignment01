<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SearchTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function check_if_search_is_possible_directly_by_isbn()
    {
        $this->actingAs(factory(\App\Models\User::class)->states('admin')->make()); //as an admin

        $book = factory(\App\Models\Book::class)->make();

        $response = $this->get('/book/?q=isbn::'.$book->isbn);

        $response->assertSee($book->isbn);
    }
}
